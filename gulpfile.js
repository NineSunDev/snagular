var gulp = require('gulp');
var rm   = require('gulp-rm');
var sass = require('gulp-sass');
var sMap = require('gulp-sourcemaps');
var sync = require('browser-sync');

var webpack  = require('webpack');
var gWebpack = require('webpack-stream');

var libraries = [
  './node_modules/react/dist/react.js',
  './node_modules/react-dom/dist/react-dom.js'
];


var swallowError = function (e) {
  // If you want details of the error in the console
  console.log(e.toString());
  this.emit('end')
};

gulp.task('default', [
  'build',
  'sync'
], function () {
  gulp.watch('./src/resources/**/*', ['copy-resources-wrapper']);
  gulp.watch('./src/app/stylesheets/**/*.scss', ['build-stylesheets-wrapper']);
  gulp.watch(['./src/**/*', '!./src/app/stylesheets/**/*'], ['build-ts-wrapper']);
});

gulp.task('build-prod', [
  'build-ts',
  'build-stylesheets',
  'copy-libraries',
  'copy-resources'
]);

gulp.task('build', [
  'build-ts',
  'build-stylesheets',
  'copy-libraries',
  'copy-resources'
], function(){
  sync.reload();
});

gulp.task('sync', function(){
  sync.init({
    server : {
      baseDir : "./dist"
    }
  });
});


gulp.task('clean-ts', function () {
  return gulp.src(['dist/bundle.js'], {read : false})
    .pipe(rm());
});
gulp.task('build-ts', ['clean-ts'], function() {
  return gulp.src('./src/app/index.ts')
    .pipe(gWebpack(require('./webpack/webpack.dev.js'), webpack))
    .on('error', swallowError)
    .pipe(gulp.dest('./dist'));
});
gulp.task('build-ts-wrapper', ['build-ts'], function(){
  sync.reload();
});


gulp.task('clean-resources', function () {
  return gulp.src(['./dist/**/*!*.js'], {read : false})
    .pipe(rm());
});
gulp.task('copy-resources', ['clean-resources'], function () {
  return gulp.src(['./src/resources/**/*'])
    .pipe(gulp.dest('./dist'));
});
gulp.task('copy-resources-wrapper', ['copy-resources'], function () {
  sync.reload();
});


gulp.task('clean-libraries', function () {
  return gulp.src('./dist/libraries/**/*', {read : false})
    .pipe(rm());
});
gulp.task('copy-libraries', ['clean-libraries'], function () {
  return gulp.src(libraries)
    .pipe(gulp.dest('./dist/libraries'));
});
gulp.task('copy-libraries-wrapper', ['copy-libraries'], function () {
  sync.reload();
});


gulp.task('clean-stylesheets', function() {
  return gulp.src('./dist/main.css', {read : false})
    .pipe(rm());
});
gulp.task('build-stylesheets', ['clean-stylesheets'], function () {
  return gulp.src('./src/app/stylesheets/main.scss')
    .pipe(sMap.init())
    .pipe(sass({outputStyle : 'compressed'}).on('error', sass.logError))
    .pipe(sMap.write())
    .pipe(gulp.dest('./dist'));
});
gulp.task('build-stylesheets-wrapper', ['build-stylesheets'], function () {
  sync.reload();
});
