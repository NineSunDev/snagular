import { ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { GameStateRecord, GameStatusService, IGameState } from '../../../../provider/game-status/game-status.service';
import {
  HighScoreService,
  HighScoreStateRecord,
  IHighScore,
  IHighScoreState,
} from '../../../../provider/high-score/high-score.service';

@Component({
  selector: 'high-scores',
  templateUrl: './high-scores.component.html',
  styleUrls: ['./high-scores.component.scss'],
})
export class HighScoresComponent {

  public gameState: IGameState        = GameStateRecord;
  public highScoresState: IHighScoreState = HighScoreStateRecord;
  public noHighScores: boolean = false;

  constructor(
    private gss: GameStatusService,
    private hss: HighScoreService,
    private cdr: ChangeDetectorRef) {

    this.hss.loadHighScores();

    this.gss.getState().subscribe((state) => {
      if (this.gameState !== state)
        this.gameState = state;
    });
    this.hss.state.subscribe((state: IHighScoreState) => {
      if (this.highScoresState !== state) {
        this.highScoresState = state;
        this.noHighScores = this.highScoresState.highScores.length === 0;
      }
    });
  }

  public trackByFn(index: number, item: IHighScore) {
    return item.id;
  }

  public back() {
    this.gss.mutate({screen: this.gameState.lastScreen});
  }
}
