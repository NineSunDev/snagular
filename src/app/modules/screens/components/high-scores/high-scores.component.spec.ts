import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HighScoresComponent } from './high-scores.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HelperModule } from '../../../helper/helper.module';
import { GameStatusService } from '../../../../provider/game-status/game-status.service';
import { HighScoreService } from '../../../../provider/high-score/high-score.service';

describe('HighScoresComponent', () => {
  let component: HighScoresComponent;
  let fixture: ComponentFixture<HighScoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HighScoresComponent ],
      imports: [
        FormsModule,
        BrowserModule,
        HttpClientModule,
        HelperModule
      ],
      providers: [
        GameStatusService,
        HighScoreService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HighScoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
