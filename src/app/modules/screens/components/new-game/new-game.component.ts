import { Component } from '@angular/core';
import { EScreens, GameStatusService } from '../../../../provider/game-status/game-status.service';

@Component({
  selector: 'new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent {

  constructor(
    private gss: GameStatusService
  ) { }

  newGame() { this.gss.mutate({screen: null, score: 0}); }
  options() { this.gss.mutate({screen: EScreens.Options}); }
  highScores() { this.gss.mutate({screen: EScreens.HighScores}); }

}
