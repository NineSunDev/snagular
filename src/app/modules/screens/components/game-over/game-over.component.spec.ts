import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameOverComponent } from './game-over.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { GameStatusService } from '../../../../provider/game-status/game-status.service';
import {
  HighScoreService,
} from '../../../../provider/high-score/high-score.service';

describe('GameOverComponent', () => {
  let component: GameOverComponent;
  let fixture: ComponentFixture<GameOverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GameOverComponent,
      ],
      imports: [
        FormsModule,
        BrowserModule,
        HttpClientModule,
      ],
      providers: [
        GameStatusService,
        HighScoreService,
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture   = TestBed.createComponent(GameOverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return correct rating for a score of 0', () => {
    expect(component.getRating(0)).toEqual(component.ratings[0]);
  });

  it('should return correct rating for a score of 1', () => {
    component.hsState = {
      pages: 1,
      entriesPerPage: 10,
      highScores: [
        {name: '3', score: 3},
        {name: '2', score: 2},
        {name: '1', score: 1},
        {name: '0', score: 0},
      ],
    };
    expect(component.getRating(1)).toEqual(component.ratings[7]);
  });

  it('should return correct rating for a score of 1000000000000', () => {
    component.hsState = {
      pages: 1,
      entriesPerPage: 10,
      highScores: [
        {name: '3', score: 3},
        {name: '2', score: 2},
        {name: '1', score: 1},
        {name: '0', score: 0},
      ],
    };
    expect(component.getRating(1000000000000)).toEqual(component.ratings[8]);
  });
});
