import { Component } from '@angular/core';
import {
  EScreens,
  GameStateRecord,
  GameStatusService,
  IGameState,
} from '../../../../provider/game-status/game-status.service';
import {
  HighScoreService,
  HighScoreStateRecord,
  IHighScoreState,
} from '../../../../provider/high-score/high-score.service';

@Component({
  selector: 'game-over',
  templateUrl: './game-over.component.html',
  styleUrls: ['./game-over.component.scss'],
})

export class GameOverComponent {

  public state: IGameState = GameStateRecord;
  public rating: string    = '';
  public name              = '';
  public saved             = false;
  public ratings: string[] = [
    'How could you be this bad?',                                               // 0 0 Points
    'Congratulations, your are part of the lowest 1% !',                        // 1 lowest 1%
    'Not that bad. Only 90% of all players are better than you.',               // 2 lowest 10%
    'OK, just below the average',                                               // 3 below 49 %
    'Nice, you are average',                                                    // 4 49 to 51 %
    'Getting there, just above average',                                        // 5 below 90 %
    'Top 10 %, you have a live?',                                               // 6 below 99 %
    'Top 10, you certainly have no live.',                                      // 7 top 10
    'Highscore! There is no way you have a live.',                              // 8 Top 1
  ];

  public hsState: IHighScoreState = HighScoreStateRecord;

  constructor(
    private gss: GameStatusService,
    private hss: HighScoreService,
  ) {
    this.hss.loadHighScores();
    this.gss.getState().subscribe((state) => {
      this.state = state;
    });
    this.hss.state.subscribe((state) => {
      this.hsState = state;
      if (!this.saved)
        this.rating = this.getRating(this.state.score);
    });
  }

  saveHighScore() {
    if (this.name.trim().length < 1) return;

    this.saved = true;
    this.hss.addHighScore({name: this.name.trim(), score: this.state.score});
  }

  mainMenu() {
    this.gss.mutate({screen: EScreens.NewGame});
  }

  getRating(score: number): string {
    if (score === 0) return this.ratings[0];

    if (this.hsState.highScores.length > 0) {
      const highscores = this.hsState.highScores.length;
      let position     = -1;

      for (let i = 0; i < highscores; i++) {
        if (this.hsState.highScores[i].score < score) {
          position = i + 1;
          break;
        }
      }

      const scoresAbove = position / highscores;

      if (position === 1) return this.ratings[8];
      if (position <= 10) return this.ratings[7];
      if (scoresAbove <= 1) return this.ratings[6];
      if (scoresAbove <= 10) return this.ratings[5];
      if (scoresAbove <= 49) return this.ratings[4];
      if (scoresAbove <= 51) return this.ratings[3];
      if (scoresAbove <= 90) return this.ratings[2];
      if (scoresAbove <= 99) return this.ratings[1];
      return this.ratings[0];

    }

    return 'You\'ve set the first Highscore. Thanks.';
  }
}
