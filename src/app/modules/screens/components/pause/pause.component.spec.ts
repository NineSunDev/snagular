import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PauseComponent } from './pause.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { GameStatusService } from '../../../../provider/game-status/game-status.service';

describe('PauseComponent', () => {
  let component: PauseComponent;
  let fixture: ComponentFixture<PauseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PauseComponent ],
      imports: [
        FormsModule,
        BrowserModule,
        HttpClientModule
      ],
      providers: [
        GameStatusService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PauseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
