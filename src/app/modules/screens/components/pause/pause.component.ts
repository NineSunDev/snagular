import { Component } from '@angular/core';
import { EScreens, GameStatusService } from '../../../../provider/game-status/game-status.service';

@Component({
  selector: 'pause',
  templateUrl: './pause.component.html',
  styleUrls: ['./pause.component.scss']
})
export class PauseComponent {

  constructor(
    private gss: GameStatusService
  ) { }

  resume() { this.gss.mutate({screen: null}); }
  quit() { this.gss.mutate({screen: EScreens.NewGame}); }
  options() { this.gss.mutate({screen: EScreens.Options}); }
  highScores() { this.gss.mutate({screen: EScreens.HighScores}); }
}
