import { Component, OnInit } from '@angular/core';
import { GameStateRecord, GameStatusService, IGameState } from '../../../../provider/game-status/game-status.service';
import { GameLoopService } from '../../../engine/provider/game-loop/game-loop.service';

@Component({
  selector: 'options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent {

  public state: IGameState = GameStateRecord;
  public targetFPS: number = 30;

  constructor(
    public gameLoop: GameLoopService,
    private gss: GameStatusService) {
    this.gss.getState().subscribe((state) => {
      this.state = state;
    });
  }

  setGameSpeed(num: number) {
    this.gss.mutate({speed: num});
  }

  setFPS(num: number) {
    this.targetFPS = (num !== 0 ? num : 30);
    this.gameLoop.timestep = 1000 / this.targetFPS;
  }

  toggleDPad() {
    this.gss.mutate({dPad: !this.state.dPad});
  }

  back() {
    this.gss.mutate({screen: this.state.lastScreen});
  }
}
