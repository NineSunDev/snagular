import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameOverComponent } from './components/game-over/game-over.component';
import { HighScoresComponent } from './components/high-scores/high-scores.component';
import { NewGameComponent } from './components/new-game/new-game.component';
import { OptionsComponent } from './components/options/options.component';
import { PauseComponent } from './components/pause/pause.component';
import { HttpClientModule } from '@angular/common/http';
import { OptionsService } from '../../provider/options/options.service';
import { HighScoreService } from '../../provider/high-score/high-score.service';
import { GameStatusService } from '../../provider/game-status/game-status.service';
import { KeyEventService } from '../../provider/key-event/key-event.service';
import { FormsModule } from '@angular/forms';
import { HelperModule } from '../helper/helper.module';

@NgModule({
  declarations: [
    GameOverComponent,
    HighScoresComponent,
    NewGameComponent,
    OptionsComponent,
    PauseComponent,
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    HelperModule,
  ],
  exports: [
    GameOverComponent,
    HighScoresComponent,
    NewGameComponent,
    OptionsComponent,
    PauseComponent,
  ],
  providers: [
    KeyEventService,
    OptionsService,
    GameStatusService,
    HighScoreService,
  ]
})
export class ScreensModule {
}
