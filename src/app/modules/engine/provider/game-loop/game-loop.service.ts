import { Injectable } from '@angular/core';

export interface ITimerCallback {
  name: string;
  callback?: { (deltaTime: number): void; };
}

@Injectable()
export class GameLoopService {

  private steps: number    = 0;
  private delta: number    = 0;

  public timestep: number = 1000 / 30;

  public fps: number               = 30;
  private framesThisSecond: number = 0;
  private lastFpsUpdate: number    = 0;

  private lastTick: number = 0;

  private callbacks: ITimerCallback[] = [];

  constructor() {

    // TODO: https://isaacsukin.com/news/2015/01/detailed-explanation-javascript-game-loops-and-timing

  }

  public start() {
    this.loop(0, this.callbacks);
  }

  private loop(_timestamp: number, callbacks: ITimerCallback[]) {
    if (_timestamp < this.lastTick + this.timestep) {
      requestAnimationFrame((timestamp) => {
        this.loop(timestamp, callbacks)
      });
      return;
    }

    // Track the accumulated time that hasn't been simulated yet
    this.delta += _timestamp - this.lastTick; // note += here
    this.lastTick = _timestamp;

    if (_timestamp > this.lastFpsUpdate + 1000) { // update every second
      this.fps = Math.ceil(0.25 * this.framesThisSecond + (1 - 0.25) * this.fps); // compute the new FPS

      this.lastFpsUpdate    = _timestamp;
      this.framesThisSecond = 0;
    }
    this.framesThisSecond++;

    // Simulate the total elapsed time in fixed-size chunks
    while (this.delta >= this.timestep) {
      for (let callback of this.callbacks) {
        callback.callback(this.timestep);
      }

      this.delta -= this.timestep;

      if (++this.steps >= 240) {
        this.delta = 0; // fix things
        break; // bail out
      }
    }
    requestAnimationFrame((timestamp) => {
      this.loop(timestamp, callbacks)
    });
  }

  public addCallback(_callback: ITimerCallback) {
    for (let callback of this.callbacks)
      if (callback.name === _callback.name)
        return;

    this.callbacks.push(_callback);
  }

  public removeCallback(_callback: ITimerCallback) {
    let id = -1;
    for (let iterator in this.callbacks) {
      if (this.callbacks[iterator].name === _callback.name) {
        id = Number(iterator);
        break;
      }
    }
    this.callbacks.slice(id);
  }

}
