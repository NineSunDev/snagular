import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameLoopService } from './provider/game-loop/game-loop.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  exports: [],
})
export class EngineModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: EngineModule,
      providers: [GameLoopService],
    }
  }
}
