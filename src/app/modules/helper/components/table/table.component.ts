import { Component, ContentChild, Input, OnInit, TemplateRef, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'helper-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnInit {

  @Input() public classTable: string;
  @Input() public data: any[];
  @Input() public headerClick: Function;
  @Input() public trackByFunction: Function;
  @Input() public pageSize: number;

  @ContentChild('head') private templateHead: TemplateRef<any>;
  @ContentChild('body') private templateBody: TemplateRef<any>;

  public pagedData: Array<any> = [];
  public currentPage: number = 0;
  public pages: number     = 0;

  constructor() { }

  ngOnInit() {
    this.pageData();
  }

  private pageData(): any[] {
    if (this.pageSize === 0) {
      this.pages = 0;
      return this.data;
    }

    this.pages = Math.floor(this.data.length / this.pageSize);
    this.pagedData = this.data.slice(this.pageSize * this.currentPage, this.pageSize * this.currentPage + this.pageSize);
  }


  onHeaderClick(e: FocusEvent) {
    let target: Element = (e.target || e.srcElement) as Element;

    if (this.headerClick) {
      this.headerClick(target.closest('th').getAttribute('data-property'));
    }
  }

  trackByFn(index: number, item: any) {
    if (this.trackByFunction) {
      return this.trackByFunction(index, item);
    }
  }

  pageBack() {
    this.currentPage = this.currentPage > 0 ? this.currentPage - 1 : 0;
    this.pageData();
  }

  pageNext() {
    this.currentPage = this.currentPage < this.pages ? this.currentPage + 1 : this.pages;
    this.pageData();
  }
}
