import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { IndexPage } from './pages/index-page/index.page';
import { GamePage } from './pages/game-page/game.page';
import { DPadComponent } from './components/d-pad/d-pad.component';
import { OptionsService } from './provider/options/options.service';
import { KeyEventService } from './provider/key-event/key-event.service';
import { GameStatusService } from './provider/game-status/game-status.service';
import { HighScoreService } from './provider/high-score/high-score.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ScreensModule } from './modules/screens/screens.module';
import { HelperModule } from './modules/helper/helper.module';
import { EngineModule } from './modules/engine/engine.module';


@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    DPadComponent,
    IndexPage,
    GamePage,
  ],
  imports: [
    EngineModule.forRoot(),
    ScreensModule,
    HelperModule,
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    KeyEventService,
    OptionsService,
    GameStatusService,
    HighScoreService,
  ],
})
export class AppModule {
}
