import { TestBed, inject } from '@angular/core/testing';

import { KeyEventService } from './key-event.service';

describe('KeyEventService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KeyEventService]
    });
  });

  it('should be created', inject([KeyEventService], (service: KeyEventService) => {
    expect(service).toBeTruthy();
  }));
});
