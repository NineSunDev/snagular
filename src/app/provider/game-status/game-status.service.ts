import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export enum EScreens {
  NewGame = 'new-game',
  Pause = 'pause',
  GameOver = 'game-over',
  HighScores = 'high-scores',
  Options = 'options'
}

export interface IMutableGameState {
  score?: number;
  screen?: EScreens;
  lastScreen?: EScreens;
  dPad?: boolean;
  speed?: number;
}

export interface IGameState {
  score: number;
  screen: EScreens;
  lastScreen: EScreens;
  dPad: boolean;

  speed: number;
}

export const GameStateRecord: IGameState = {
  score: 0,
  screen: EScreens.NewGame,
  lastScreen: null,
  dPad: false,
  speed: 5,
};

@Injectable()
export class GameStatusService {

  private state: BehaviorSubject<IGameState> = new BehaviorSubject(GameStateRecord);

  constructor() {
  }

  public getState() {
    return this.state
  };

  public mutate(_state: IMutableGameState) {
    let oldState = this.state.value;

    for (let key in _state) {
      if (_state.hasOwnProperty(key))
        if (oldState.hasOwnProperty(key)) {
          if (key === 'screen') {
            oldState.lastScreen = oldState.screen;
          }

          oldState[key] = _state[key];
        }
    }

    this.state.next(oldState);
  }
}
