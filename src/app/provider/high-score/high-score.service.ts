import { Injectable, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';
import { IMutableGameState } from '../game-status/game-status.service';

export interface IHighScore {
  id?: number;
  name: string;
  score: number;
  date?: string;
}

export interface IHighScoreState {
  highScores: IHighScore[];
  pages: number;
  entriesPerPage: number;
}

export interface IHighScoreMutableState {
  highScores?: IHighScore[];
  pages?: number;
  entriesPerPage?: number;
}

export const HighScoreStateRecord : IHighScoreState = {
  highScores: [],
  pages: 0,
  entriesPerPage: 10
};

@Injectable()
export class HighScoreService {

  public state: BehaviorSubject<IHighScoreState> = new BehaviorSubject<IHighScoreState>(HighScoreStateRecord);

  private serverURI: string = isDevMode() ? 'http://localhost:8067/' : 'https://api.ninesun.de/';
  private apiURI: string = 'snake/high-scores';

  constructor(private http: HttpClient) { }


  public mutate(state: IHighScoreMutableState) {
    let mutatedState = this.state.value;

    for (let key in state) {
      if (state.hasOwnProperty(key))
        if (mutatedState.hasOwnProperty(key)) {
          mutatedState[key] = state[key];
        }
    }

    this.state.next(mutatedState);
  }

  public loadHighScores() {
    this.http.get(this.serverURI + this.apiURI).subscribe((data: any) => {
      this.mutate({highScores: data.items});
    });
  }

  public addHighScore(highScore: IHighScore) {
    this.http.post(this.serverURI + this.apiURI, {name: highScore.name, score: highScore.score}).subscribe(() => {
      this.loadHighScores();
    });
  }

}
