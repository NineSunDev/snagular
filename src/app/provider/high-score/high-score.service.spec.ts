import { TestBed, inject } from '@angular/core/testing';

import { HighScoreService } from './high-score.service';
import { HttpClientModule } from '@angular/common/http';

describe('HighScoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HighScoreService],
      imports: [
        HttpClientModule
      ]
    });
  });

  it('should be created', inject([HighScoreService], (service: HighScoreService) => {
    expect(service).toBeTruthy();
  }));
});
