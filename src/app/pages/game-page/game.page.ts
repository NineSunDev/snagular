import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { KeyEventService, UnListen } from '../../provider/key-event/key-event.service';
import {
  GameStateRecord,
  GameStatusService,
  IGameState,
  EScreens,
} from '../../provider/game-status/game-status.service';
import { ActivatedRoute } from '@angular/router';
import { GameLoopService } from '../../modules/engine/provider/game-loop/game-loop.service';

interface ISnakeHeadEntry {
  position: IVector2;
  oldPosition: IVector2;
  direction: IVector2;
}

interface ISnakeBodyEntry {
  position: IVector2;
  oldPosition: IVector2;
  frozen: number;
  headID: number;
  tailID: number;
}

interface IItem {
  position: IVector2;
  points: number;
  bonus: number;
  ttl: number;
}

interface IVector2 {
  x: number;
  y: number;
}

@Component({
  selector: 'gamePage',
  template: require('./game.page.html'),
  styles: [require('./game.page.scss')],
  host: {
    '(window:resize)': 'calculateGameArea()',
  },
})
export class GamePage implements OnInit {

  @ViewChild('gameCanvas') gameCanvas: ElementRef;
  @ViewChild('snakehead') snakeHeadRef: ElementRef;

  public gameArea: IVector2  = {x: 0, y: 0};
  public dimension: IVector2 = {x: 20, y: 20};
  public dPadVisible         = false;

  public eScreens          = EScreens;
  public state: IGameState = GameStateRecord;

  private defaultHead: ISnakeHeadEntry = {
    position: {x: 0, y: 0},
    oldPosition: {x: 0, y: 0},
    direction: {x: 1, y: 0},
  };
  private head: ISnakeHeadEntry        = null;
  public body: ISnakeBodyEntry[]       = [];
  public activeItems: IItem[]          = [];

  private timer: number = 0;
  private unListen: any;

  constructor(
    private kep: KeyEventService,
    private game: GameStatusService,
    private route: ActivatedRoute,
    public gameLoop: GameLoopService,
  ) {
  }


  ngOnInit() {
    this.unListen = this.kep.listen({
        // Movement
        'w': this.changeDirectionUp.bind(this),
        'a': this.changeDirectionLeft.bind(this),
        's': this.changeDirectionDown.bind(this),
        'd': this.changeDirectionRight.bind(this),

        'Up': this.changeDirectionUp.bind(this),
        'Left': this.changeDirectionLeft.bind(this),
        'Down': this.changeDirectionDown.bind(this),
        'Right': this.changeDirectionRight.bind(this),

        // Game management
        'Space': this.resumeGame.bind(this),
        'Esc': this.togglePause.bind(this),
      },
      {
        priority: 0,
      },
    );

    this.game.getState().subscribe((state) => {
      if (this.state !== state)
        this.state = state;

      if (this.state.screen === EScreens.NewGame) {
        this.calculateGameArea();
        this.start();
      }
    });

    this.route.params.subscribe((params) => {
      const screen: EScreens = params['screen'];

      if (Object.values(EScreens).includes(screen)) {
        this.game.mutate({screen: screen});
      }

      if (this.state.screen === EScreens.NewGame) {
        this.calculateGameArea();
        this.start();
      }
    });

    this.calculateGameArea();
  }

  start() {
    this.gameLoop.removeCallback({name: 'snangular.game.page.tick'});

    this.resetTimer();
    this.activeItems   = [];
    this.body          = [];
    this.head          = Object.create(this.defaultHead);
    this.head.position = {x: 0, y: 0};

    this.gameLoop.addCallback({
      name: 'snangular.game.page.tick',
      callback: (delta: number) => {
        if ((this.timer -= delta) <= 0) {
          this.tick();
          this.resetTimer();
        }
      },
    });
  }

  private resetTimer() {
    this.timer = 1000 / (this.state.speed <= 0 ? 1 : this.state.speed);
  }

  tick() {
    if (this.state.screen !== EScreens.GameOver) {
      if (this.state.screen === null) {
        this.activeItems = this.activeItems.filter((item) => {
          return item.ttl > 1;
        });

        if (this.activeItems.length < 2 && Math.round(Math.random())) {
          this.spawnRandomItem();
        }

        this.moveSnakeHead();
        this.moveSnakeBody();

        this.eatItems();
      }
    }
  }


  calculateGameArea() {
    const maxWidth  = window.innerWidth <= 1024 ? window.innerWidth : window.innerWidth * 0.9;
    const maxHeight = window.innerHeight * 0.66;

    const maxColumns = Math.floor(maxWidth / this.dimension.x);
    const maxRows    = Math.floor(maxHeight / this.dimension.y);

    this.gameArea = {
      x: maxColumns * this.dimension.x,
      y: maxRows * this.dimension.y,
    };
  }

  toggleDPad() {
    this.dPadVisible = !this.dPadVisible;
  }


  openOptions() {
    this.game.mutate({screen: EScreens.Options})
  }


  eatItems() {
    this.activeItems = this.activeItems.filter((item) => {
      if (item.position.x === this.head.position.x && item.position.y === this.head.position.y) {
        this.game.mutate({score: this.state.score + item.points * this.state.speed});
        this.addBodySegment();

        return false;
      }
      return true;
    })
  }

  spawnRandomItem() {
    const maxX = this.gameArea.x / this.dimension.x;
    const maxY = this.gameArea.y / this.dimension.y;

    const rndX = Math.floor(Math.random() * maxX);
    const rndY = Math.floor(Math.random() * maxY);

    if (this.testPosition({x: rndX, y: rndY}))
      this.activeItems.push({
        position: {x: rndX * this.dimension.x, y: rndY * this.dimension.y},
        bonus: 1,
        points: 100,
        ttl: 15,
      })
  }

  addBodySegment() {
    const newPosition: IVector2 = this.body.length > 0 ?
      this.body[this.body.length - 1].position : this.head.position;

    if (this.body.length !== 0) {
      this.body[this.body.length - 1].tailID = this.body.length;
    }

    this.body.push({
      headID: this.body.length > 0 ? this.body.length - 1 : -1,
      position: Object.assign({}, newPosition),
      oldPosition: Object.assign({}, newPosition),
      tailID: null,
      frozen: 1,
    });

  }

  togglePause() {
    if (this.state.screen == EScreens.Pause) {
      this.resumeGame();
    }else if (this.state.screen == null) {
      this.pauseGame();
    }
  }

  resumeGame() {
    this.calculateGameArea();
    this.game.mutate({screen: null});
  }

  pauseGame() {
    if (this.state.screen !== EScreens.GameOver)
      this.game.mutate({screen: EScreens.Pause});
  }


  changeDirectionLeft() {
    if (this.body.length > 0 &&
      !this.testPosition({
        x: this.head.position.x - this.dimension.x,
        y: this.head.position.y,
      })) {
      return;
    }
    this.head.direction = {x: -1, y: 0};
  }

  changeDirectionUp() {
    if (this.body.length > 0 &&
      !this.testPosition({
        x: this.head.position.x,
        y: this.head.position.y - this.dimension.y,
      })) {
      return;
    }
    this.head.direction = {x: 0, y: -1};
  }

  changeDirectionRight() {
    if (this.body.length > 0 &&
      !this.testPosition({
        x: this.head.position.x + this.dimension.x,
        y: this.head.position.y,
      })) {
      return;
    }
    this.head.direction = {x: 1, y: 0};
  }

  changeDirectionDown() {
    if (this.body.length > 0 &&
      !this.testPosition({
        x: this.head.position.x,
        y: this.head.position.y + this.dimension.y,
      })) {
      return;
    }
    this.head.direction = {x: 0, y: 1};
  }


  private moveSnakeBody() {
    for (const iter in this.body) {
      if (this.body[iter].frozen != 0) {
        this.body[iter].frozen--;
        return;
      }

      this.body[iter].oldPosition = Object.assign({}, this.body[iter].position);

      this.body[iter].position = this.body[iter].headID === -1
        ? Object.assign({}, this.head.oldPosition)
        : Object.assign({}, this.body[this.body[iter].headID].oldPosition);
    }

  }

  private moveSnakeHead() {
    this.head.oldPosition = Object.assign({}, this.head.position);

    const newX: number = this.head.position.x + this.dimension.x * this.head.direction.x;
    const newY: number = this.head.position.y + this.dimension.y * this.head.direction.y;

    if (!this.testPosition({x: newX, y: newY})) {
      this.game.mutate({screen: EScreens.GameOver});
      return;
    }

    this.head.position.x = newX;
    this.head.position.y = newY;
  }

  testPosition(pos: IVector2): boolean {
    // Out of area
    if (pos.x >= this.gameArea.x || pos.y >= this.gameArea.y
      || pos.x < 0 || pos.y < 0) {
      return false;
    }

    for (const segment of this.body) {
      if (segment.position.x === pos.x && segment.position.y === pos.y) {
        return false;
      }
    }

    return !(this.head.position.x === pos.x && this.head.position.y === pos.y);
  }
}
