import { Component } from '@angular/core';
import { GameLoopService } from '../../modules/engine/provider/game-loop/game-loop.service';

@Component({
  selector: 'snangular',
  template: require('./app.component.html'),
  styles: [ require('./app.component.scss') ]
})
export class AppComponent {

  constructor(
    private gameLoop: GameLoopService
  ) {
    gameLoop.start();
  }
}
