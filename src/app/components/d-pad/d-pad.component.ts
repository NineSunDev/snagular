import { Component, Input } from '@angular/core';

@Component({
  selector: 'd-pad',
  template: require('./d-pad.component.html'),
  styles: [ require('./d-pad.component.scss') ]
})
export class DPadComponent {

  @Input() handlerUp: any;
  @Input() handlerDown: any;
  @Input() handlerLeft: any;
  @Input() handlerRight: any;

  constructor() {
  }

  onClickUp() { this.handlerUp(); }
  onClickDown() { this.handlerDown(); }
  onClickLeft() { this.handlerLeft(); }
  onClickRight() { this.handlerRight(); }
}
