import { NgModule } from '@angular/core';

import {
  RouterModule,
  Routes
} from '@angular/router';
import { GamePage } from './pages/game-page/game.page';

const appRoutes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/play'
  },
  {
    path: 'play',
    component: GamePage
  },
  {
    path: 'play/:screen',
    component: GamePage
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
