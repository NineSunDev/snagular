var webpack = require('webpack');
var path = require('path');
var helpers = require('./helpers.js');

module.exports = {
  entry  : helpers.root("/src/app/bootstrap.ts"),
  output : {
    filename : "main.js",
    path     : helpers.root("/dist")
  },

  mode: process.env.NODE_ENV ? process.env.NODE_ENV : 'development',

  resolve : {
    extensions : ['.ts', '.js', '.json'],
    modules : [helpers.root('src'), helpers.root('node_modules')]
  },

  module : {

    rules : [

      /*
       * Typescript loader support for .ts
       *
       * Component Template/Style integration using `angular2-template-loader`
       * Angular 2 lazy loading (async routes) via `ng-router-loader`
       *
       * `ng-router-loader` expects vanilla JavaScript code, not TypeScript code. This is why the
       * order of the loader matter.
       *
       * See: https://github.com/s-panferov/awesome-typescript-loader
       * See: https://github.com/TheLarkInn/angular2-template-loader
       * See: https://github.com/shlomiassaf/ng-router-loader
       */
      {
        test    : /\.ts$/,
        use     : [
          { // MAKE SURE TO CHAIN VANILLA JS CODE, I.E. TS COMPILATION OUTPUT.
            loader  : 'ng-router-loader',
            options : {
              loader : 'async-import',
              genDir : 'compiled'
            }
          },
          {
            loader  : 'awesome-typescript-loader',
            options : {
              configFileName : 'tsconfig.json'
            }
          },
          {
            loader : 'angular2-template-loader'
          }
        ],
        exclude : [/\.(spec|e2e)\.ts$/, /node_modules/]
      },

      /*
       * Json loader support for *.json files.
       *
       * See: https://github.com/webpack/json-loader
       */
      {
        test : /\.json$/,
        use  : 'json-loader'
      },

      /*
       * to string and css loader support for *.css files (from Angular components)
       * Returns file content as string
       *
       */
      {
        test    : /\.css$/,
        use     : ['to-string-loader', 'css-loader'],
        exclude : [helpers.root('src', 'styles')]
      },

      /*
       * to string and sass loader support for *.scss files (from Angular components)
       * Returns compiled css content as string
       *
       */
      {
        test    : /\.scss$/,
        use     : ['to-string-loader', 'css-loader', 'sass-loader'],
        exclude : [helpers.root('src', 'styles')]
      },

      /* Raw loader support for *.html
       * Returns file content as string
       *
       * See: https://github.com/webpack/raw-loader
       */
      {
        test    : /\.html$/,
        use     : 'raw-loader',
        exclude : [helpers.root('src/index.html')]
      },

      /*
       * File loader for supporting images, for example, in CSS files.
       */
      {
        test : /\.(jpg|png|gif)$/,
        use  : 'file-loader'
      },

      /* File loader for supporting fonts, for example, in CSS files.
       */
      {
        test : /\.(eot|woff2?|svg|ttf)([\?]?.*)$/,
        use  : 'file-loader'
      }

    ]

  },

  plugins : [
    new webpack.ContextReplacementPlugin(/\@angular(\\|\/)core(\\|\/)esm5/, path.join(__dirname, './client'))
  ],


  node : {
    global         : true,
    crypto         : 'empty',
    process        : true,
    module         : false,
    clearImmediate : false,
    setImmediate   : false
  }
};
